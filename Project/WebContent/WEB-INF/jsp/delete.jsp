<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>ユーザ削除確認</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
crossorigin="anonymous">
<link rel="stylesheet" href="css/delete.css">
</head>
<body>
<header>
<div class="row">
    <p class="name">${userInfo.name} さん</p>
    <a href="LogoutServlet" class="logout">ログアウト</a>
</div>
</header>
<div align="center"><h1>ユーザ削除確認</h1></div>

<div align="center">
<table border="0">
<form action="DeleteServlet" method="post">
<input type="hidden" name="id" value="${userDetail.id }">

<h4 class="h42">ログインID :${userDetail.loginId }</h4>
<h4>を本当に削除してもよろしいでしょうか？</h4>
<div class="row">
<tr><td class="toroku" colspan="2"><a href="UserListServlet" class="button">キャンセル</a><td class="toroku" colspan="2"><input type="submit" value="OK" class="button"></td>
    </tr></div>
</form>
</table>
</div>

</body>
</html>