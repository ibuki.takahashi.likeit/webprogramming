package controller;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
		if(user1==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

	    String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo3(id);

		request.setAttribute("userDetail", user);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

	// リクエストパラメータの入力項目を取得
    String id = request.getParameter("id");
	String password = request.getParameter("password");
	String password2 = request.getParameter("password2");
	String userName = request.getParameter("user_name");
	String birth = request.getParameter("birth");
	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
	UserDao userDao = new UserDao();

	if(!password.equals(password2)) {
		request.setAttribute("errMsg", "入力された内容は正しくありません。");

		// ログインjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
		return;
	}

	if(
userName.equals("")&&birth.equals("")) {

		request.setAttribute("errMsg", "入力された内容は正しくありません。");

		// ログインjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
		return;
	}



	if(password.equals("")&&password2.equals("")) {
	userDao.UpdatepasswordLess(id,userName, birth);
	}
	else{
		String password1=userDao.password(password);
		userDao.UserUpdate(id,password1, userName, birth);}

	response.sendRedirect("UserListServlet");


}
}

