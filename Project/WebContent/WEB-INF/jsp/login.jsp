<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>ログイン</title>
<link rel="stylesheet" href="css/login.css">
 <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
<body>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<div align="center"><h1>ログイン画面</h1></div>

<div align="center">
<table border="0">
 <form class="form-signin" action="LoginServlet" method="post">

<tr><th>ログインID　</th>
<td><input type="text" name="loginId" value="" size="24"></td>
</tr>
<tr><th>パスワード　</th>
<td><input type="password" name="password" value="" size="24"></td>
</tr>
<tr><th></th><td colspan="2"><input type="submit" value="ログイン" class="button"></td>
</tr>
</form>
</table>
</div>
</body>
</html>