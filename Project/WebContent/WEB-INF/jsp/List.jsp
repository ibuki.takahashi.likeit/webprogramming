<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>ユーザ一覧</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

<link rel="stylesheet" href="css/List.css">
</head>
<body>
 <header>

<div class="row">
    <p class="name">${userInfo.name} さん </p>
    <a href="LogoutServlet" class="logout">ログアウト</a>
</div>
</header>

<div align="center"><h1>ユーザ一覧</h1></div>

<a href="CreateServlet" class="createaccount">新規登録</a>

<div align="center">
<table border="0">
<form class="form-signin" action="UserListServlet" method="post">

<tr><th>ログインID　</th>
<td><input type="text" name="loginId" value="" size="24"></td>
</tr>
<tr><th>ユーザ名　</th>
<td><input type="username" name="username" value="" size="24"></td>
</tr>
<tr><th>生年月日　</th>
<div class="row">
<div class="col-sm-2">
    <td><input type="date" name="date-start" id="date-start" class="form-control" size="30"/></td>
</div>
<div class="col-xs-1 text-center"><td>
    ~</td>
</div>
<div class="col-sm-2">
    <td><input type="date" name="date-end" id="date-end" class="form-control"/></td>
</div>
</div>

<tr><th></th><td colspan="2"><input type="submit" value="検索" class="button"></td>
</tr>

</form>
</table>    <hr size="2">
<table>
<thead>
  <tr>
    <th class="color">ログインID</th> <th class="color">ユーザ名 </th> <th class="color">生年月日</th><th class="color" colspan="2"></th>
  </tr>
</thead>
   <tbody>
  <c:forEach var="user" items="${userList}" >
 <tr>
<td>${user.loginId}</td>
 <td>${user.name}</td>
 <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
 <td>
 <a class="btn btn-primary" href="DetailServlet?id=${user.id}">詳細</a>
 <c:if test="${userInfo.loginId==user.loginId||userInfo.loginId=='admin'}">
 <a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
 </c:if>
 <c:if test="${userInfo.loginId=='admin'}">
 <a class="btn btn-danger" href ="DeleteServlet?id=${user.id}">削除</a>
 </c:if>
 </td>
 </tr>
 </c:forEach>
 </tbody>
</div>
</body>
</html>