<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>ユーザ新規登録</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
crossorigin="anonymous">
<link rel="stylesheet" href="css/createaccount.css">
</head>
<body>
<header>
<div class="row">
    <p class="name">ユーザ名　さん</p>
    <a href="LogoutServlet" class="logout">ログアウト</a>
</div>
</header>
<div align="center"><h1>ユーザ新規登録</h1></div>
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<div align="center">
<table border="0">
<form class="form-createdate" action="CreateServlet" method="post">

<tr><th>ログインID　</th>
<td ><input type="text" name="loginId" value="" size="24"></td>
</tr>
    <tr><th> </th></tr>
<tr><th class="sp1">パスワード</th>
<td class="sp"><input type="password" name="password" value="" size="24"></td>
</tr>
<tr><th class="sp1">パスワード(確認)　</th>
<td class="sp"><input type="password2" name="password2" value="" size="24"></td>
</tr>
<tr><th class="sp1">ユーザ名　</th>
<td class="sp"><input type="text" name="user_name" value="" size="24"></td>
</tr>
<tr><th class="sp1">生年月日　</th>
<td class="sp"><input type="text" name="birth" value="" size="24"></td>
</tr>
<tr><th></th><td class="toroku" colspan="2"><input type="submit" value="登録" class="button"></td>
</tr>
</form>
</table>
</div>
<footer>
<a href="UserListServlet" class="return">戻る</a>

</footer>
</body>
</html>