<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>ユーザ情報詳細参照</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
crossorigin="anonymous">
<link rel="stylesheet" href="css/detail.css">
</head>
<body>
<header>
<div class="row">
    <p class="name">${userInfo.name} さん </p>
    <a href="LogoutServlet" class="logout">ログアウト</a>
</div>
</header>
<div align="center"><h1>ユーザ情報詳細参照</h1></div>

<div align="center">
<table border="0">
<form action=".html" method="get">

<tr><th>ログインID　</th>
<td >${userDetail.loginId}</td>
</tr>
    <tr><th> </th></tr>
<tr><th class="sp1">ユーザ名</th>
<td class="sp">${userDetail.name}</td>
</tr>
<tr><th class="sp1">生年月日</th>
<td class="sp">${userDetail.birthDate}</td>
</tr>
<tr><th class="sp1">登録日時　</th>
<td class="sp">${userDetail.createDate}</td>
</tr>
<tr><th class="sp1">更新日時　</th>
<td class="sp">${userDetail.updateDate}</td>
</tr>

</form>
</table>
</div>
<footer>
<a href="UserListServlet" class="return">戻る</a>

</footer>
</body>
</html>